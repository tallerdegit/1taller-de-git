public class SayayinDTO {
  private String nombre;
  private Integer nivelPoder;
  private static final Integer OVER_POWERED = 8000;

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public Integer getNivelPoder() {
    return nivelPoder;
  }

  public void setNivelPoder(Integer nivelPoder) {
    this.nivelPoder = nivelPoder;
  }

  
}
